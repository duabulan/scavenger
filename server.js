#!/bin/env node
//  OpenShift sample Node application
var http = require('http');

//Get the environment variables we need.
var server_ip_address  = process.env.OPENSHIFT_NODEJS_IP || "127.0.0.1";
var server_port    = process.env.OPENSHIFT_NODEJS_PORT || 8080;

var express = require('express');
var app = express();

app.get('/', function(req, res){
  res.send('Hello World');
});

var server = app.listen(server_port, server_ip_address, function() {
    console.log("Server running at http://" + server_ip_address + ":" + server_port + "/");
});